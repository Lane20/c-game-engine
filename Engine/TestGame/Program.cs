﻿using Engine;
using Engine.Mathematics;
using System.Linq;

namespace TestGame {
    class Program {
        class Sword : GameItem {
            public Sword(string iconKey) : base(iconKey) { }

            public override GameObject CreateGameObject(Point point) {
                return new SwordGameObject(point);
            }
        }

        class SwordGameObject : GameObject {
            public SwordGameObject(Point point) : base(point) {
                image = Textures.GetTexture("Error");
            }

            public override GameItem CreateGameItem() {
                return new Sword("Error");
            }
        }

        static void Main(string[] args) {
            DirectXManager.Initialize();

            var gameInfo = new GameInfo {
                StartPlayerCoords = new Point(200, 200),
                CanGameEnd = _ => true,
                GameEnd = _ => { }
            };

            var game = new Game(gameInfo);
            Textures.Add("Grass", DirectXManager.LoadBitmap(typeof(Program).Assembly, "Grass.png"));
            game.Controller.AddGameItem(new SwordGameObject(new Point(250, 250)));
            game.Controller.AddGameItem(new SwordGameObject(new Point(450, 450)));
            game.Controller.AddGameItem(new SwordGameObject(new Point(238, 376)));
            game.Controller.AddGameItem(new SwordGameObject(new Point(134, 20)));
            game.Controller.AddGameItem(new SwordGameObject(new Point(35, 400)));
            game.AchievementRegistrator.Register("3 Items Get!", "Pick up 3 items", x => x.Player.Inventory.Count >= 3);
            game.AchievementRegistrator.Register("5 Items Get!", "Pick up 5 items", x => x.Player.Inventory.Count >= 5);
            game.Light.Setup(30 * 1000);
            game.StartGame();
        }
    }
}