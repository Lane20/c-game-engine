﻿using System.Collections.Generic;

namespace Engine {
    public class Inventory {
        public Inventory(LiveObject owner, Game game, int capacity = 16) {
            this.owner = owner;
            this.game = game;
            items = new List<GameItem>();
            Capacity = capacity;
        }

        readonly LiveObject owner;
        readonly Game game;
        readonly IList<GameItem> items;
        public IEnumerable<GameItem> Items => items;
        public int Capacity { get; set; }
        public int Count => items.Count;

        public bool Put(GameItem item) {
            if(items.Count == Capacity)
                return false;
            items.Add(item);
            return true;
        }

        public bool Pull(int index) {
            if(index > items.Count)
                return false;
            var gameObject = items[index].CreateGameObject(owner.Point.Clone());
            items.RemoveAt(index);
            game.Controller.AddGameItem(gameObject);
            return true;
        }
    }
}