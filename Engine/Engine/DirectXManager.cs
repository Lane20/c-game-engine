﻿using System;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using Engine.Core;
using Engine.Settings;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Windows;
using AlphaMode = SharpDX.Direct2D1.AlphaMode;
using Bitmap = SharpDX.Direct2D1.Bitmap;
using Device = SharpDX.Direct3D11.Device;
using Factory = SharpDX.Direct2D1.Factory;
using Factory1 = SharpDX.DXGI.Factory1;
using FeatureLevel = SharpDX.Direct2D1.FeatureLevel;

namespace Engine {
    public static class DirectXManager {
        public static RenderTarget RenderTarget;
        public static SharpDX.DirectWrite.TextFormat DebugTextFormat;

        static RenderForm RenderForm;
        static Device Device;
        static SwapChain SwapChain;
        static SharpDX.WIC.ImagingFactory2 ImagingFactory2;
        static RenderLoop RenderLoop;
        static int SyncInterval;

        public static void Initialize() {
            RenderForm = new RenderForm(Game.Title).Store();
            RenderForm.ClientSize = new Size(Game.Width, Game.Height);
            var sampleDescription = new SampleDescription(1, 0);
            var modeDescription = new ModeDescription(0, 0, new Rational(60, 1), Format.R8G8B8A8_UNorm);
            var swapChainDesc = new SwapChainDescription() {
                BufferCount = 2,
                Usage = Usage.RenderTargetOutput,
                OutputHandle = RenderForm.Handle,
                IsWindowed = true,
                ModeDescription = modeDescription,
                SampleDescription = sampleDescription,
                Flags = SwapChainFlags.AllowModeSwitch,
                SwapEffect = SwapEffect.Discard
            };
            using(var factory = new Factory1()) {
                var adapter = factory.Adapters1[0];
                var featureLevels = new[] {
                    SharpDX.Direct3D.FeatureLevel.Level_11_0,
                    SharpDX.Direct3D.FeatureLevel.Level_10_1,
                    SharpDX.Direct3D.FeatureLevel.Level_10_0,
                    SharpDX.Direct3D.FeatureLevel.Level_9_3,
                };
                Device.CreateWithSwapChain(adapter, DeviceCreationFlags.Debug | DeviceCreationFlags.BgraSupport, featureLevels, swapChainDesc, out Device, out SwapChain);
                adapter.Dispose();
            }
            var backBuffer = Surface.FromSwapChain(SwapChain, 0).Store();
            using(var factory = new Factory()) {
                var dpi = factory.DesktopDpi;
                var renderTargetProperties = new RenderTargetProperties() {
                    DpiX = dpi.Width,
                    DpiY = dpi.Height,
                    MinLevel = FeatureLevel.Level_DEFAULT,
                    PixelFormat = new PixelFormat(Format.R8G8B8A8_UNorm, AlphaMode.Ignore),
                    Type = RenderTargetType.Default,
                    Usage = RenderTargetUsage.None
                };
                RenderTarget = new RenderTarget(factory, backBuffer, renderTargetProperties).Store();
            }
            using(var factory = SwapChain.GetParent<Factory1>())
                factory.MakeWindowAssociation(RenderForm.Handle, WindowAssociationFlags.IgnoreAll);

            using(var fontFactory = new SharpDX.DirectWrite.Factory()) {
                DebugTextFormat = new SharpDX.DirectWrite.TextFormat(fontFactory, FontFamily.GenericMonospace.Name, 12f).Store();
            }
            ImagingFactory2 = new SharpDX.WIC.ImagingFactory2();
            RenderLoop = new RenderLoop(RenderForm).Store();
        }

        public static void Render(Action render) {
            RenderTarget.BeginDraw();
            render();
            RenderTarget.EndDraw();
            SwapChain.Present(SyncInterval, PresentFlags.None);
            RenderLoop.NextFrame();
        }

        public static void RegisterWindowEvent(string eventName, Action action) {
            RegisterWindowEvent(eventName, (EventArgs x) => action());
        }

        public static void RegisterWindowEvent<T>(string eventName, Action<T> action) where T : EventArgs {
            EventHandlerHelper.Register(RenderForm, eventName, action);
        }

        public static Bitmap ConvertImage(this System.Drawing.Bitmap image) {
            var sourceArea = new System.Drawing.Rectangle(0, 0, image.Size.Width, image.Size.Height);
            var size = new Size(image.Width, image.Height);
            var pixelFormat = new PixelFormat(Format.R8G8B8A8_UNorm, AlphaMode.Premultiplied);
            var bitmapProperties = new BitmapProperties(pixelFormat);
            int stride = image.Width * sizeof(int);
            using(var tempStream = new DataStream(image.Height * stride, true, true)) {
                var bitmapData = image.LockBits(sourceArea, System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
                for(int y = 0; y < image.Height; y++) {
                    int offset = bitmapData.Stride * y;
                    for(int x = 0; x < image.Width; x++) {
                        byte B = Marshal.ReadByte(bitmapData.Scan0, offset++);
                        byte G = Marshal.ReadByte(bitmapData.Scan0, offset++);
                        byte R = Marshal.ReadByte(bitmapData.Scan0, offset++);
                        byte A = Marshal.ReadByte(bitmapData.Scan0, offset++);
                        int rgba = R | (G << 8) | (B << 16) | (A << 24);
                        tempStream.Write(rgba);
                    }

                }
                image.UnlockBits(bitmapData);
                tempStream.Position = 0;
                return new Bitmap(RenderTarget, new Size2(size.Width, size.Height), tempStream, stride, bitmapProperties).Store();
            }
        }

        public static Bitmap LoadBitmap(Assembly assembly, string path) {
            Bitmap bitmap;
            using(var stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.{path}")) {
                var bitmapDecoder = new SharpDX.WIC.BitmapDecoder(
                    ImagingFactory2,
                    stream,
                    SharpDX.WIC.DecodeOptions.CacheOnDemand
                );
                var result = new SharpDX.WIC.FormatConverter(ImagingFactory2);
                result.Initialize(
                    bitmapDecoder.GetFrame(0),
                    SharpDX.WIC.PixelFormat.Format32bppPRGBA,
                    SharpDX.WIC.BitmapDitherType.None,
                    null,
                    0.0,
                    SharpDX.WIC.BitmapPaletteType.Custom
                );
                bitmap = Bitmap.FromWicBitmap(RenderTarget, result);
            }
            return bitmap;
        }

        public static void ShowWindow() {
            RenderForm.Show();
        }

        public static void DisposeForm() {
            RenderForm.Dispose();
        }

        public static void Configure(VideoSettings settings) {
            SwapChain.SetFullscreenState(settings.IsFullScreen, null);
            SyncInterval = settings.VSync ? 1 : 0;
        }
    }
}