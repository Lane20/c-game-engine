﻿using Point = Engine.Mathematics.Point;

namespace Engine {
    public class Pistol : Weapon {
        // Need static array for all weapons with times to shoot
        public Pistol(GameObject owner, Point point, Point tempPoint, Game game, int bulletWidth, int bulletHeight) : base(owner, point, 4) {
            this.tempPoint = tempPoint;
            this.game = game;
            this.bulletWidth = bulletWidth;
            this.bulletHeight = bulletHeight;
        }

        readonly Point tempPoint;
        readonly Game game;
        readonly int bulletWidth;
        readonly int bulletHeight;

        public override void Shoot() {
            var bullet = new Bullet(new Point(owner.Point.X + owner.Width / 2, owner.Point.Y + owner.Height / 2), new Point(tempPoint), bulletWidth, bulletHeight);
            game.Controller.AddBullet(bullet);
        }
    }
}