﻿using System;

namespace Engine.Debugging {
    public static class DebugHelper {
        public static void ShowCoordsObject(GameObject obj) {
            var bounds = obj.GetRenderBounds();
            var renderX = bounds.Left;
            var renderY = bounds.Top;
            DirectXManager.RenderTarget.DrawText("X: " + obj.Point.X, DirectXManager.DebugTextFormat, new SharpDX.Mathematics.Interop.RawRectangleF(renderX, (float)renderY - 25, (float)renderX + 100, (float)renderY - 5), Brushes.GetBrush(SharpDX.Color.White));
            DirectXManager.RenderTarget.DrawText("Y: " + obj.Point.Y, DirectXManager.DebugTextFormat, new SharpDX.Mathematics.Interop.RawRectangleF(renderX, (float)renderY - 15, (float)renderX + 100, (float)renderY + 5), Brushes.GetBrush(SharpDX.Color.White));
        }

        public static void ShowBounds(GameObject obj) {
            var bounds = obj.GetRenderBounds();
            DirectXManager.RenderTarget.DrawRectangle(new SharpDX.Mathematics.Interop.RawRectangleF(bounds.Left, bounds.Top, bounds.Right, bounds.Bottom), Brushes.GetBrush(SharpDX.Color.Red), 2);
        }

        public static void ShowLineBetweenObjects(GameObject objA, GameObject objB) {
            DirectXManager.RenderTarget.DrawLine(new SharpDX.Mathematics.Interop.RawVector2((float)objA.Point.X, (float)objA.Point.Y), new SharpDX.Mathematics.Interop.RawVector2((float)objB.Point.X, (float)objB.Point.Y), Brushes.GetBrush(SharpDX.Color.White));
        }

        public static void ShowRegionWhereCanPickUpItems(GameObject obj, int radius) {
            var bounds = obj.GetRenderBounds();
            DirectXManager.RenderTarget.DrawEllipse(new SharpDX.Direct2D1.Ellipse(new SharpDX.Mathematics.Interop.RawVector2(bounds.X + bounds.Width / 2, bounds.Y + bounds.Height / 2), radius + bounds.Width / 2, radius + bounds.Height / 2), Brushes.GetBrush(SharpDX.Color.Yellow));
            var brush = new SharpDX.Direct2D1.SolidColorBrush(DirectXManager.RenderTarget, SharpDX.Color.Yellow) { Opacity = 0.1f };
            DirectXManager.RenderTarget.FillEllipse(new SharpDX.Direct2D1.Ellipse(new SharpDX.Mathematics.Interop.RawVector2(bounds.X + bounds.Width / 2, bounds.Y + bounds.Height / 2), radius + bounds.Width / 2, radius + bounds.Height / 2), brush);
            brush.Dispose();
        }

        public static void ThrowErrorMessage(string message) {
            ThrowMessageCore(message, ConsoleColor.Red);
        }

        public static void ThrowInfoMessage(string message) {
            ThrowMessageCore(message, ConsoleColor.Cyan);
        }

        static void ThrowMessageCore(string message, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}