﻿using Engine.Mathematics;
using SharpDX.Direct2D1;

namespace Engine {
    public abstract class GameItem {
        protected GameItem(string iconKey) {
            Icon = Textures.GetTexture(iconKey);
        }

        public Bitmap Icon { get; }

        public abstract GameObject CreateGameObject(Point point);
    }
}