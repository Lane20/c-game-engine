﻿using SharpDX.Direct2D1;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public class Tile : GameObject {
        public Tile(Bitmap image, Point point) : base(point) {
            this.image = image;
        }

        public int CellWidth { get; private set; }
        public int CellHeight { get; private set; }

        public void SetupSettings(int cellWidth, int cellHeight) {
            CellWidth = cellWidth < 1 ? 32 : cellWidth;
            CellHeight = cellHeight < 1 ? 32 : cellHeight;
            image = Textures.GetTexture("Error");
        }

        protected override void RenderCore(Point renderPoint) {
            DirectXManager.RenderTarget.DrawBitmap(image, new SharpDX.Mathematics.Interop.RawRectangleF((float)renderPoint.X, (float)renderPoint.Y, (float)renderPoint.X + image.Size.Width, (float)renderPoint.Y + image.Size.Height), 1f, BitmapInterpolationMode.Linear);
        }
    }
}