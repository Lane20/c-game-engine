﻿using System.Drawing;
using Bitmap = SharpDX.Direct2D1.Bitmap;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public abstract class RenderedObject {
        public Point Point { get; set; } = Point.Null;
        public float Width => image.Size.Width;
        public float Height => image.Size.Height;
        Point RenderPoint => new Point(Point.X - Camera.X, Point.Y - Camera.Y);

        protected Bitmap image;

        public void Render() {
            RenderCore(RenderPoint);
        }
        public Rectangle GetBounds() {
            return new Rectangle((int)Point.X, (int)Point.Y, (int)Width, (int)Height);
        }
        public Rectangle GetRenderBounds() {
            var renderPoint = RenderPoint;
            return new Rectangle((int)renderPoint.X, (int)renderPoint.Y, (int)Width, (int)Height);
        }
        protected abstract void RenderCore(Point renderPoint);
    }
}