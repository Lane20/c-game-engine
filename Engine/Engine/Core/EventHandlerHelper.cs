﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Engine.Core {
    public static class EventHandlerHelper {
        static readonly IDictionary<object, IList<KeyValuePair<string, IList<Action<EventArgs>>>>> Controls;

        static EventHandlerHelper() {
            Controls = new Dictionary<object, IList<KeyValuePair<string, IList<Action<EventArgs>>>>>();
        }

        public static void Register(object obj, string eventName, Action action) {
            Register(obj, eventName, (EventArgs x) => action());
        }

        public static void Register<T>(object obj, string eventName, Action<T> action) where T : EventArgs {
            IList<KeyValuePair<string, IList<Action<EventArgs>>>> events;
            if(!Controls.TryGetValue(obj, out events))
                Controls.Add(obj, new List<KeyValuePair<string, IList<Action<EventArgs>>>>());
            events = Controls[obj];
            if(!events.Select(x => x.Key).Contains(eventName)) {
                var actions = new List<Action<EventArgs>>();
                events.Add(new KeyValuePair<string, IList<Action<EventArgs>>>(eventName, actions));
                Action<T> registryAction = x => {
                    foreach(var act in actions)
                        act(x);
                };
                var eventInfo = obj.GetType().GetEvent(eventName);
                var eventParams = eventInfo.EventHandlerType.GetMethod("Invoke").GetParameters();
                var parameters = new[] {
                    Expression.Parameter(typeof(object), "sender"),
                    Expression.Parameter(eventParams[1].ParameterType, "e")
                };
                var body = Expression.Call(Expression.Constant(registryAction), registryAction.GetType().GetMethod("Invoke"), parameters[1]);
                var lambda = Expression.Lambda(body, parameters[0], parameters[1]);
                eventInfo.AddEventHandler(obj, Delegate.CreateDelegate(eventInfo.EventHandlerType, lambda.Compile(), "Invoke", false));
            }
            var existingActions = events.First(x => x.Key == eventName).Value;
            existingActions.Add(x => action(x as T));
        }
    }
}