﻿using System;
using System.Collections.Generic;

namespace Engine.Core {
    public static class DisposeHelper {
        static readonly IList<IDisposable> Objects;

        static DisposeHelper() {
            Objects = new List<IDisposable>();
        }

        public static T Store<T>(this T obj) where T : IDisposable {
            Objects.Add(obj);
            return obj;
        }

        public static void Dispose(IDisposable obj) {
            obj?.Dispose();
            Objects.Remove(obj);
        }

        public static void Dispose(params IDisposable[] objects) {
            for(int i = objects.Length; i-- > 0;)
                Dispose(objects[i]);
        }

        public static void DisposeStorage() {
            for(int i = Objects.Count; i-- > 0;)
                Dispose(Objects[i]);
        }
    }
}