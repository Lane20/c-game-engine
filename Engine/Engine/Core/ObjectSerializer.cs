﻿#if VLAD_FIX_THIS_CODE_PLEASE
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace Engine.Core
{
    static class ObjectSerializer<T>
    {
        static SQLiteConnectionStringBuilder db = new SQLiteConnectionStringBuilder();
        public static void Load()
        {
            db.DataSource = "Base";
            using (SQLiteConnection connection = new SQLiteConnection(db.ConnectionString))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(connection))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = @"SELECT * FROM Items";
                    using (SQLiteDataReader Reader = command.ExecuteReader())
                    {
                        while (Reader.Read())
                        {
                            Console.Write("  " + Reader.GetInt32(0) + "\t | " + Reader.GetString(1) + "\t | " + Reader.GetInt32(2) + "\n");
                        }
                    }
                }
                connection.Close();
            }
        }
        public static void Save(T Item)
        {
            db.DataSource = "Base";
            using (SQLiteConnection connection = new SQLiteConnection(db.ConnectionString))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(connection))
                {
                    command.CommandText = @"INSERT INTO Items (Name, Damage) VALUES ('" + Item.Name + "', " + Item.Damage + ");";
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }
    }
}
#endif