﻿using System;

namespace Engine.PathFinding {
    public interface IHeapItem : IComparable {
        int GetHeapIndex();
        void SetHeapIndex(int index);
    }
}