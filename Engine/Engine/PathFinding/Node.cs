﻿using System;

namespace Engine.PathFinding {
    public class Node : IHeapItem {
        public Node(int gridX, int gridY) {
            GridX = gridX;
            GridY = gridY;
            GCost = 0;
            HCost = 0;
        }

        int heapIndex;

        public int GridX { get; }
        public int GridY { get; }
        public int GCost { get; set; }
        public int HCost { get; set; }
        public Node Parent { get; set; }

        public int GetFCost() {
            return GCost + HCost;
        }

        int CompareInt(int a, int b) {
            if(a < b)
                return -1;
            if(a > b)
                return 1;
            return 0;
        }

        #region IComparable

        int IComparable.CompareTo(object o) {
            Node node = (Node)o;
            int compare = CompareInt(GetFCost(), node.GetFCost());
            if(compare == 0)
                compare = CompareInt(HCost, node.HCost);
            return -compare;
        }

        #endregion

        #region IHeapItem

        int IHeapItem.GetHeapIndex() { return heapIndex; }
        void IHeapItem.SetHeapIndex(int index) { heapIndex = index; }

        #endregion
    }
}