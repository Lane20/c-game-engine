﻿namespace Engine.PathFinding {
    public class Heap {
        public Heap(int maxHeapSize) {
            items = new IHeapItem[maxHeapSize];
        }

        readonly IHeapItem[] items;
        int currentItemCount;

        public void Add(IHeapItem item) {
            item.SetHeapIndex(currentItemCount);
            items[currentItemCount] = item;
            SortUp(item);
            currentItemCount++;
        }

        public IHeapItem RemoveFirst() {
            IHeapItem firstItem = items[0];
            currentItemCount--;
            items[0] = items[currentItemCount];
            items[0].SetHeapIndex(0);
            SortDown(items[0]);
            return firstItem;
        }

        public bool Contains(IHeapItem item) {
            if(items[item.GetHeapIndex()] == null)
                return false;
            return items[item.GetHeapIndex()].Equals(item);
        }

        public void UpdateItem(IHeapItem item) { SortUp(item); }

        public int GetCount() { return currentItemCount; }

        void SortDown(IHeapItem item) {
            while(true) {
                int childIndexLeft = item.GetHeapIndex() * 2 + 1;
                int childIndexRight = item.GetHeapIndex() * 2 + 2;
                if(childIndexLeft < currentItemCount) {
                    var swapIndex = childIndexLeft;
                    if(childIndexRight < currentItemCount)
                        if(items[childIndexLeft].CompareTo(items[childIndexRight]) < 0)
                            swapIndex = childIndexRight;
                    if(item.CompareTo(items[swapIndex]) < 0)
                        Swap(item, items[swapIndex]);
                    else
                        return;
                } else {
                    return;
                }
            }
        }

        void SortUp(IHeapItem item) {
            int parentIndex = (item.GetHeapIndex() - 1) / 2;

            while(true) {
                IHeapItem parentItem = items[parentIndex];
                if(item.CompareTo(parentItem) > 0)
                    Swap(item, parentItem);
                else
                    break;
                parentIndex = (item.GetHeapIndex() - 1) / 2;
            }
        }

        void Swap(IHeapItem itemA, IHeapItem itemB) {
            items[itemA.GetHeapIndex()] = itemB;
            items[itemB.GetHeapIndex()] = itemA;
            int itemAIndex = itemA.GetHeapIndex();
            itemA.SetHeapIndex(itemB.GetHeapIndex());
            itemB.SetHeapIndex(itemAIndex);
        }
    }
}