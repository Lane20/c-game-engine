﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine.Mathematics;

namespace Engine.PathFinding {
    public class PathFinder {
        public static int XPixels = 5;
        public static int YPixels = 5;

        static Node[][] NodeArray;
        //NEED REFACTORING
        public static void InitializeNodeArray() {
            NodeArray = new Node[Game.Width / XPixels][];
            for(int i = 0; i < Game.Width / XPixels; i++) {
                NodeArray[i] = new Node[Game.Height / YPixels];
                for(int j = 0; j < Game.Height / YPixels; j++)
                    NodeArray[i][j] = new Node(i, j);
            }
        }

        public static LinkedList<Node> GetNeighbours(Node node) {
            LinkedList<Node> neighbours = new LinkedList<Node>();
            for(int i = -1; i < 2; i++) {
                for(int j = -1; j < 2; j++) {
                    if(i == 0 && j == 0)
                        continue;

                    int checkX = node.GridX + i;
                    int checkY = node.GridY + j;

                    if(checkX >= 0 && checkX < Game.Width / XPixels && checkY >= 0 && checkY < Game.Height / YPixels)
                        neighbours.AddLast(NodeArray[checkX][checkY]);
                }
            }

            return neighbours;
        }

        public static void FindPath(Point start, Point targer, Enemy obj) {
            Node startNode = NodeArray[(int)(start.X / XPixels)][(int)(start.Y / YPixels)];
            Node targetNode = NodeArray[(int)(targer.X / XPixels)][(int)(targer.Y / YPixels)];

            Heap openSet = new Heap(Game.Width * Game.Height);
            LinkedList<Node> closedSet = new LinkedList<Node>();
            openSet.Add(startNode);

            while(openSet.GetCount() > 0) {
                Node currentNode = (Node)openSet.RemoveFirst();
                closedSet.AddLast(currentNode);

                if(currentNode == targetNode) {
                    obj.path = RetracePath(startNode, targetNode);
                    return;
                }

                foreach(var neighbour in GetNeighbours(currentNode)) {
                    //WALK LOGIC?
                    if(closedSet.Contains(neighbour))
                        continue;

                    int newMovementCostToNeighbour = currentNode.GCost + getDistance(currentNode, neighbour);
                    if(newMovementCostToNeighbour < neighbour.GCost || !openSet.Contains(neighbour)) {
                        neighbour.GCost = newMovementCostToNeighbour;
                        neighbour.HCost = getDistance(neighbour, targetNode);
                        neighbour.Parent = currentNode;

                        if(!openSet.Contains(neighbour)) {
                            openSet.Add(neighbour);
                        }
                    }
                }
            }
        }

        static LinkedList<Node> RetracePath(Node startNode, Node endNode) {
            LinkedList<Node> path = new LinkedList<Node>();
            Node currentNode = endNode;

            while(currentNode != startNode) {
                path.AddLast(currentNode);
                currentNode = currentNode.Parent;
            }
            LinkedList<Node> reversedList = new LinkedList<Node>();
            for(int i = path.Count - 1; i >= 0; i--)
                reversedList.AddLast(path.ElementAt(i));

            return reversedList;
        }

        public static int getDistance(Node node1, Node node2) {
            int dstX = Math.Abs(node1.GridX - node2.GridX);
            int dstY = Math.Abs(node1.GridY - node2.GridY);
            if(dstX > dstY)
                return 14 * dstY + 10 * (dstX - dstY);
            return 14 * dstX + 10 * (dstY - dstX);
        }
    }
}