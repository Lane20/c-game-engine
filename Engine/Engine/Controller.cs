﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Engine {
    public class Controller {
        public Controller() {
            //GAME FOR SPRITE?
            bullets = new List<Bullet>();
            enemies = new List<Enemy>();
            tiles = new List<Tile>();
            obstacles = new List<Obstacle>();
            blows = new List<Blow>();
            gameItems = new List<GameObject>();
        }

        readonly IList<Bullet> bullets;
        readonly IList<Enemy> enemies;
        readonly IList<Tile> tiles;
        readonly IList<Obstacle> obstacles;
        readonly IList<Blow> blows;
        readonly IList<GameObject> gameItems;
        public IEnumerable<Obstacle> Obstacles => obstacles;
        public IEnumerable<Bullet> Bullets => bullets;
        public IEnumerable<GameObject> GameItems => gameItems;

        public void AddBullet(Bullet bullet) { bullets.Add(bullet); }

        public void AddEnemy(Enemy enemy) { enemies.Add(enemy); }

        public void AddTile(Tile tile) { tiles.Add(tile); }

        public void AddObstacle(Obstacle obstacle) { obstacles.Add(obstacle); }

        public void AddBlow(Blow blow) { blows.Add(blow); }

        public void AddGameItem(GameObject obj) { gameItems.Add(obj); }

        public void RemoveBullet(Bullet bullet) { bullets.Remove(bullet); }

        public void RemoveEnemy(Enemy enemy) { enemies.Remove(enemy); }

        public void RemoveTile(Tile tile) { tiles.Remove(tile); }

        public void RemoveObstacle(Obstacle obstacle) { obstacles.Remove(obstacle); }

        public void RemoveBlow(Blow blow) { blows.Remove(blow); }

        public void RemoveGameItem(GameObject obj) { gameItems.Remove(obj); }

        public void Render() {
            RenderCore(tiles);
            RenderCore(bullets);
            RenderCore(enemies);
            RenderCore(obstacles);
            RenderCore(blows);
            RenderCore(gameItems);
        }

        public void Tick() {
            TickCore(tiles, RemoveTile);
            TickCore(bullets, RemoveBullet);
            TickCore(enemies, RemoveEnemy);
            TickCore(obstacles, RemoveObstacle);
            TickCore(gameItems, RemoveGameItem);
            TickCoreTime(blows, RemoveBlow);
        }

        void RenderCore<T>(IList<T> objects) where T : GameObject {
            var saveList = objects.ToList();
            for(int i = 0; i < saveList.Count; i++)
                saveList[i].Render();
        }

        void TickCore<T>(IList<T> objects, Action<T> removeAction) where T : GameObject {
            for(int i = 0; i < objects.Count; i++) {
                var obj = objects[i];
                if(obj.Point.X >= Game.Width + 20 || obj.Point.X <= -20)
                    removeAction(obj);
                else if(obj.Point.Y >= Game.Height + 20 || obj.Point.Y <= -20)
                    removeAction(obj);
                obj.Tick();
            }
        }
        void TickCoreTime<T>(IList<T> objects, Action<T> removeAction) where T : Blow {
            for(int i = 0; i < objects.Count; i++) {
                var obj = objects[i];
                if(DateTime.Now - obj.TimeCreate > new TimeSpan(0, 0, 0, 0, 100)) {
                    removeAction(obj);
                }
                obj.Tick();
            }
        }
    }
}