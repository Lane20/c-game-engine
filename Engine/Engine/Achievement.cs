﻿using System;

namespace Engine {
    public class Achievement {
        public Achievement(string title, string description, Func<bool> completePredicate) {
            Title = title;
            Description = description;
            this.completePredicate = completePredicate;
        }

        readonly Func<bool> completePredicate;
        public string Title { get; }
        public string Description { get; }

        bool isComplete;
        public bool IsComplete() => isComplete || (isComplete = completePredicate());
    }
}