﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Engine.Core;
using Engine.Debugging;
using Engine.PathFinding;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public sealed class Game {
        public const int Width = 1280;
        public const int Height = 720;
        public const string Title = "Prototype";
        const int Speed = 5;
        const int ObstacleCount = 10;

        static Game() {
            //Performance???
            EventHandlerHelper.Register(AppDomain.CurrentDomain, "UnhandledException", (UnhandledExceptionEventArgs e) => DebugHelper.ThrowErrorMessage((e.ExceptionObject as Exception)?.Message));
        }

        public Game() : this(GameInfo.Default) { }
        public Game(GameInfo gameInfo) {
            ti = DateTime.Now;
            grid = new Grid();
            Controller = new Controller();
            Light = new Light();
            Settings = new Settings.Settings();
            Player = new Player(Point.Null, this, new Crosshair(Point.Null));
            AchievementRegistrator = new AchievementRegistrator(this);
            endPredicate = gameInfo.CanGameEnd;
            SpawnPlayer(gameInfo.StartPlayerCoords);
            gameEndAction = gameInfo.GameEnd;
        }

        public Controller Controller { get; }
        public Light Light { get; }
        bool running;
        public Player Player { get; }
        List<Keys> pressedKeys;
        int tempFPS;
        int fps;
        readonly Grid grid;
        ///TEMP
        MapGenerator mapCreator;
        ///
        readonly Predicate<Game> endPredicate;
        readonly Action<Game> gameEndAction;
        public Settings.Settings Settings { get; }
        public AchievementRegistrator AchievementRegistrator { get; }

        public void Run() {
            Initialize();
            const long amountOfTicks = 60;
            var timePerTick = (double)TimeSpan.TicksPerSecond / amountOfTicks;
            double nextTime = DateTime.Now.Ticks;
            var updates = 0;
            var time = DateTime.Now;
            DirectXManager.ShowWindow();
            DirectXManager.Configure(Settings.Video);
            while(running) {
                if((nextTime - DateTime.Now.Ticks) <= 0) {
                    nextTime += timePerTick;
                    Tick();
                    updates++;
                }
                DirectXManager.Render(Render);
                if((DateTime.Now - time).Ticks > 10000000) {
                    time += new TimeSpan(TimeSpan.TicksPerSecond);
                    Console.WriteLine(updates + " Ticks");
                    updates = 0;
                }
                if(endPredicate(this))
                    gameEndAction?.Invoke(this);
            }
        }

        public void StartGame() {
            Start();
        }

        DateTime ti;
        void Render() {
            DirectXManager.RenderTarget.Transform = SharpDX.Matrix3x2.Identity;
            DirectXManager.RenderTarget.Clear(SharpDX.Color.Transparent);
            Controller.Render();
            Player.Render();
            Player.Crosshair.Render();
            Light.Render();
            grid.Render();
            var output = string.Join(Environment.NewLine,
                $"Player.x = {Player.Point.X}",
                $"Player.y = {Player.Point.Y}",
                $"FPS: {tempFPS}",
                $"Crosshair.x = {Player.Crosshair.Point.X}",
                $"Crosshair.y = {Player.Crosshair.Point.Y}",
                $"Grid size: = {grid.CellWidth} {grid.CellHeight}",
                $"Selected Weapon Index = {Player.Weapons.IndexOf(Player.Weapon)}",
                $"Selected Alt Weapon Index = {Player.Weapon.AltWeapons.IndexOf(Player.Weapon.AltWeapon)}",
                $"Inventory items count: {Player.Inventory.Count}",
                $"Achievement count: {Player.AchievementCount}"
            );
            DirectXManager.RenderTarget.DrawText(output, DirectXManager.DebugTextFormat, new SharpDX.Mathematics.Interop.RawRectangleF(8, 8, 225, 8), Brushes.GetBrush(SharpDX.Color.White));
            fps++;
            if((DateTime.Now - ti).Ticks > TimeSpan.TicksPerSecond) {
                ti += new TimeSpan(TimeSpan.TicksPerSecond);
                tempFPS = fps;
                fps = 0;
            }
        }

        void Start() {
            if(running)
                return;

            running = true;
            Run();
        }

        void Stop() {
            if(!running)
                return;

            running = false;
            try {
                DisposeHelper.DisposeStorage();
            } catch(Exception e) {
                Console.WriteLine(e.Message);
            }
            Application.Exit();
        }

        void Tick() {
            Player.Tick();
            Controller.Tick();
            Light.Tick();
            Player.CheckAchievements();
        }

        void Initialize() {
            DirectXManager.RegisterWindowEvent("FormClosed", Stop);
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.Left && !pressedKeys.Contains(Keys.Left)) {
                    pressedKeys.Add(Keys.Left);
                    Player.VelX = -Speed;
                }
            });
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.Up && !pressedKeys.Contains(Keys.Up)) {
                    pressedKeys.Add(Keys.Up);
                    Player.VelY = -Speed;
                }
            });
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.Right && !pressedKeys.Contains(Keys.Right)) {
                    pressedKeys.Add(Keys.Right);
                    Player.VelX = Speed;
                }
            });
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.Down && !pressedKeys.Contains(Keys.Down)) {
                    pressedKeys.Add(Keys.Down);
                    Player.VelY = Speed;
                }
            });
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.ShiftKey && !pressedKeys.Contains(Keys.ShiftKey)) {
                    pressedKeys.Add(Keys.ShiftKey);
                    Player.Speed = 2;
                }
            });
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.ControlKey && !pressedKeys.Contains(Keys.ControlKey)) {
                    pressedKeys.Add(Keys.ControlKey);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyUp", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.Left) {
                    Player.VelX = pressedKeys.Contains(Keys.Right) ? Speed : 0;
                    pressedKeys.Remove(Keys.Left);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyUp", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.ControlKey) {
                    pressedKeys.Remove(Keys.ControlKey);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyUp", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.Up) {
                    Player.VelY = pressedKeys.Contains(Keys.Down) ? Speed : 0;
                    pressedKeys.Remove(Keys.Up);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyUp", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.Right) {
                    if(pressedKeys.Contains(Keys.Left))
                        Player.VelX = -Speed;
                    else
                        Player.VelX = 0;
                    pressedKeys.Remove(Keys.Right);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyUp", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.Down) {
                    if(pressedKeys.Contains(Keys.Up))
                        Player.VelY = -Speed;
                    else
                        Player.VelY = 0;
                    pressedKeys.Remove(Keys.Down);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyUp", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.ShiftKey) {
                    Player.Speed = 1;
                    pressedKeys.Remove(Keys.ShiftKey);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyUp", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.F) {
                    var random = new Random();
                    var enemy = new Enemy(new Point(random.NextDouble() * Width, random.NextDouble() * Height), this, Player);
                    enemy.Weapon = new Pistol(enemy, enemy.Point, Player.Point, this, 8, 8);
                    Controller.AddEnemy(enemy);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyUp", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.P) {
                    grid.SetupSettings(grid.CellWidth + 5, grid.CellHeight + 5);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyUp", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.M) {
                    grid.SetupSettings(grid.CellWidth - 5, grid.CellHeight - 5);
                }
            });
            DirectXManager.RegisterWindowEvent("MouseWheel", (MouseEventArgs e) => {
                if(e.Delta > 0 && !pressedKeys.Contains(Keys.ControlKey)) {
                    Player.Weapon.Shooting = false;
                    Player.ChangeWeapon();
                } else if(e.Delta > 0 && pressedKeys.Contains(Keys.ControlKey)) {
                    Player.Weapon.AltChangeWeapon();
                }
            });
            DirectXManager.RegisterWindowEvent("MouseWheel", (MouseEventArgs e) => {
                if(e.Delta < 0 && !pressedKeys.Contains(Keys.ControlKey)) {
                    Player.Weapon.Shooting = false;
                    Player.ChangeWeapon(false);
                } else if(e.Delta < 0 && pressedKeys.Contains(Keys.ControlKey)) {
                    Player.Weapon.AltChangeWeapon(false);
                }
            });
            DirectXManager.RegisterWindowEvent("MouseMove", (MouseEventArgs e) => {
                Player.Crosshair.Point.X = e.X + Camera.X;
                Player.Crosshair.Point.Y = e.Y + Camera.Y;
            });
            DirectXManager.RegisterWindowEvent("MouseDown", (MouseEventArgs e) => {
                if(e.Button == MouseButtons.Left) {
                    Player.Weapon.Shooting = true;
                } 
            });
            DirectXManager.RegisterWindowEvent("MouseUp", (MouseEventArgs e) => {
                if(e.Button == MouseButtons.Left) {
                    Player.Weapon.Shooting = false;
                } else if((e.Button == MouseButtons.Right)) {
                    Player.Weapon.AltWeapon?.Shoot();
                }
            });
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.E) {
                    foreach(var gameItem in GetObjectsAroundPlayer(64)) {
                        Player.Inventory.Put(gameItem.CreateGameItem());
                        //It's logic must be here?
                        Controller.RemoveGameItem(gameItem);
                    }
                }
            });
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.Space && e.Control) {
                    Settings.Video.IsFullScreen = !Settings.Video.IsFullScreen;
                    DirectXManager.Configure(Settings.Video);
                }
            });
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => {
                if(e.KeyCode == Keys.V && e.Control) {
                    Settings.Video.VSync = !Settings.Video.VSync;
                    DirectXManager.Configure(Settings.Video);
                }
            });
            const int cameraStep = 10;
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => Camera.Y = e.KeyCode == Keys.W ? Camera.Y - cameraStep : Camera.Y);
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => Camera.Y = e.KeyCode == Keys.S ? Camera.Y + cameraStep : Camera.Y);
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => Camera.X = e.KeyCode == Keys.A ? Camera.X - cameraStep : Camera.X);
            DirectXManager.RegisterWindowEvent("KeyDown", (KeyEventArgs e) => Camera.X = e.KeyCode == Keys.D ? Camera.X + cameraStep : Camera.X);
            pressedKeys = new List<Keys>();
            Controller.AddObstacle(new Obstacle(new Point(300, 300), 25, 25));
            mapCreator = new MapGenerator(Controller);
            mapCreator.GenerateMap(ObstacleCount);
            grid.SetupSettings(32, 32);
            PathFinder.InitializeNodeArray();
            DebugHelper.ThrowInfoMessage("Initialize completed");
        }

        void SpawnPlayer(Point point) {
            Player.Point = point;
            Player.AddWeapon(new Pistol(Player, Player.Point, Player.Crosshair.Point, this, 8, 8));
            Player.Weapon.AddAltWeapons(new Hands(Player, this, Player.Point, 60));
            Player.Weapon.AddAltWeapons(new Pistol(Player, Player.Point, Player.Crosshair.Point, this, 16, 16));
            Player.AddWeapon(new Hands(Player, this, Player.Point, 30));
        }

        //NEED TEST FOR THIS
        IEnumerable<GameObject> GetObjectsAroundPlayer(int radius) {
            var graphicsPath = new System.Drawing.Drawing2D.GraphicsPath();
            var bounds = Player.GetBounds();
            graphicsPath.AddEllipse(
                bounds.X - radius,
                bounds.Y - radius,
                radius * 2 + bounds.Width,
                radius * 2 + bounds.Height
            );
            var region = new System.Drawing.Region(graphicsPath);
            return Controller.GameItems.Where(gameItem => region.IsVisible(gameItem.GetBounds())).ToList();
        }
    }
}