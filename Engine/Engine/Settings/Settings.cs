﻿namespace Engine.Settings {
    public class Settings {
        public Settings() {
            Video = new VideoSettings();
        }

        public VideoSettings Video { get; }

        // NEED GRAPHICS SETTINGS
        // NEED AUDIO SETTINGS
        // NEED GAME SETTINGS
        // NEED NETWORK SETTINGS
        // NEED CONTROLS SETTINGS
    }
}