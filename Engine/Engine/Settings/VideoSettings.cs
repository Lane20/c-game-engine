﻿namespace Engine.Settings {
    public class VideoSettings {
        public VideoSettings() {
            IsFullScreen = false;
            VSync = false;
        }

        public bool IsFullScreen { get; set; }
        public bool VSync { get; set; }
    }
}