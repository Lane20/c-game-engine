﻿using System.Drawing;

namespace Engine {
    public class Grid : RenderedObject {

        public Grid() {
            SetupSettings(32, 32);
        }

        public int CellWidth { get; private set; }
        public int CellHeight { get; private set; }

        public void SetupSettings(int cellWidth, int cellHeight) {
            CellWidth = cellWidth < 1 ? 32 : cellWidth;
            CellHeight = cellHeight < 1 ? 32 : cellHeight;
            var bitmap = new Bitmap(Game.Width, Game.Height);
            using(var graphics = Graphics.FromImage(bitmap)) {
                graphics.Clear(Color.Transparent);
                var currentX = 0;
                while((currentX += CellWidth) < Game.Width)
                    graphics.DrawLine(Pens.Gray, currentX, 0, currentX, Game.Height);
                var currentY = 0;
                while((currentY += CellHeight) < Game.Height)
                    graphics.DrawLine(Pens.Gray, 0, currentY, Game.Width, currentY);
                graphics.DrawImage(bitmap, 0, 0);
            }
            image?.Dispose();
            image = bitmap.ConvertImage();
            bitmap.Dispose();
        }

        protected override void RenderCore(Mathematics.Point renderPoint) {
            DirectXManager.RenderTarget.DrawBitmap(image, new SharpDX.Mathematics.Interop.RawRectangleF(0, 0, Game.Width, Game.Height), 1f, SharpDX.Direct2D1.BitmapInterpolationMode.Linear);
        }
    }
}