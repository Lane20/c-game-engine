﻿using System;

namespace Engine.Mathematics {
    public static class MathHelper {
        public const float Epsilon = 0.000001f;

        public static double GetNextCoord(double a, double b, double steps) {
            return Math.Abs(a - b) > Epsilon ? ((b - a) / steps) : 0;
        }

        public static double GetSteps(Vector vector, double speed) {
            return vector.Lenght / speed;
        }

        public static double GetScalarProduct(Vector a, Vector b) {
            return a.X * b.X + a.Y * b.Y;
        }

        public static double GetAngleBetweenVectors(Vector a, Vector b) {
            return Math.Acos(GetScalarProduct(a, b) / (a.Lenght * b.Lenght));
        }

        public static double GetRotateTheta(Point a, Point b) {
            return Math.Atan2(a.Y - b.Y, a.X - b.X) - Math.PI / 2;
        }

        public static double RadianToDegree(double radian) {
            return radian * 180 / Math.PI;
        }
    }
}