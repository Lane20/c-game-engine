﻿namespace Engine.Mathematics {
    public class Coords {
        public Coords(double x, double y) {
            X = x;
            Y = y;
        }

        public double X { get; set; }
        public double Y { get; set; }
    }
}