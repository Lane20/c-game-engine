﻿using System;

namespace Engine.Mathematics {
    public class Vector : Coords {
        public Vector(Vector vector) : this(vector.A.Clone(), vector.B.Clone()) { }
        public Vector(Point a, Point b) : base(b.X - a.X, b.Y - a.Y) {
            A = a;
            B = b;
            Lenght = Math.Sqrt(X * X + Y * Y);
        }

        public static Vector Empty = new Vector(Point.Null, Point.Null);

        public Point A { get; }
        public Point B { get; }
        public double Lenght { get; }
    }
}