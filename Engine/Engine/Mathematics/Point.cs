﻿namespace Engine.Mathematics {
    public class Point : Coords {
        public Point(Coords point) : this(point.X, point.Y) { }
        public Point(double x, double y) : base(x, y) { }

        public static Point Null => new Point(0, 0).Clone();

        public Point Clone() { return new Point(X, Y); }
    }
}