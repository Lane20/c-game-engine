﻿using System;
using System.Collections.Generic;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public abstract class Weapon : GameObject {
        protected Weapon(GameObject owner, Point point, int shotsPerSecond) : base(point) {
            this.owner = owner;
            // Increase logic for time
            timePerOneShoot = new TimeSpan(0, 0, 0, 0, 1000 / shotsPerSecond);
            nextTime = DateTime.Now - timePerOneShoot;
            //
            Shooting = false;
            AltWeapons = new List<Weapon>();
        }

        readonly TimeSpan timePerOneShoot;
        protected readonly GameObject owner;
        DateTime nextTime;

        public double VelX { get; set; }
        public double VelY { get; set; }
        public bool Shooting { private get; set; }
        public Weapon AltWeapon { get; set; }
        //TODO: Maybe it must been public?
        public IList<Weapon> AltWeapons;

        public override void Tick() {
            if(Shooting) {
                if((nextTime - DateTime.Now).TotalMilliseconds <= 0) {
                    nextTime = DateTime.Now + timePerOneShoot;
                    Shoot();
                }
            }
        }

        public abstract void Shoot();

        public void AddAltWeapons(Weapon weapon) {
            AltWeapons.Add(weapon);
            AltWeapon = weapon;
        }

        public void RemoveAltWeapons(Weapon weapon) {
            AltWeapons.Remove(weapon);
            AltWeapon = (AltWeapons.Count > 0) ? AltWeapons[0] : null;
        }
        public void AltChangeWeapon(bool next = true) {
            if(AltWeapons.Count > 0) {
                var index = AltWeapons.IndexOf(AltWeapon) + (next ? 1 : -1);
                AltWeapon = AltWeapons[index < 0 ? AltWeapons.Count - 1 : index % AltWeapons.Count];
            }
        }
    }
}