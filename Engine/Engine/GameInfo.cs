﻿using System;
using Engine.Mathematics;

namespace Engine {
    public class GameInfo {
        static GameInfo() {
            Default = new GameInfo {
                StartPlayerCoords = Point.Null,
                CanGameEnd = _ => true,
                GameEnd = _ => { }
            };
        }

        public static readonly GameInfo Default;

        public Point StartPlayerCoords { get; set; }
        public Predicate<Game> CanGameEnd { get; set; }
        public Action<Game> GameEnd { get; set; }
    }
}