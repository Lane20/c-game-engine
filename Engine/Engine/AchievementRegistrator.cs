﻿using System;
using System.Collections.Generic;

namespace Engine {
    public class AchievementRegistrator {
        public AchievementRegistrator(Game game) {
            this.game = game;
            achievements = new List<Achievement>();
        }

        readonly Game game;
        readonly IList<Achievement> achievements;
        public IEnumerable<Achievement> Achievements => achievements;

        public void Register(string title, string description, Predicate<Game> completeCondition) {
            achievements.Add(new Achievement(title, description, () => completeCondition(game)));
        }
    }
}