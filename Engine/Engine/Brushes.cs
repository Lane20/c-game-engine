﻿using System.Collections.Concurrent;
using Engine.Core;
using SharpDX;
using SharpDX.Direct2D1;

namespace Engine {
    public static class Brushes {
        static Brushes() {
            Storage = new ConcurrentDictionary<Color, SolidColorBrush>();
        }

        static readonly ConcurrentDictionary<Color, SolidColorBrush> Storage;

        public static Brush GetBrush(Color color) {
            return Storage.GetOrAdd(color, x => new SolidColorBrush(DirectXManager.RenderTarget, x).Store());
        }
    }
}