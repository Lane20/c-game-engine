﻿using System;
using System.Drawing;
using Engine.Debugging;

namespace Engine {
    public class ImageLoader {
        public static Image Error = LoadErrorImage();

        public static Image LoadImage(string path) {
            try {
                var stream = typeof(ImageLoader).Assembly.GetManifestResourceStream($"Engine.{path}");
                if(stream == null)
                    throw new InvalidOperationException();
                return Image.FromStream(stream);
            } catch {
                DebugHelper.ThrowErrorMessage("Can't load image: " + path);
                return Error;
            }
        }

        public static Image LoadImage(string path, int width, int height) {
            var image = new Bitmap(width, height);
            using(var graphics = Graphics.FromImage(image)) {
                graphics.DrawImage(LoadImage(path), 0, 0, width, height);
            }
            return image;
        }


        static Image LoadErrorImage() {
            try {
                return LoadImage("Images.Error.png");
            } catch {
                DebugHelper.ThrowErrorMessage("Can't load error image");
                return new Bitmap(1, 1);
            }
        }
    }
}