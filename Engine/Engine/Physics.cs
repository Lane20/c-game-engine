﻿using System.Linq;

namespace Engine {
    public class Physics {
        public static bool Collision(GameObject objA, GameObject objB) {
            return objA.GetBounds().IntersectsWith(objB.GetBounds());
        }

        public static bool Collision(GameObject objA, GameObject[] listObjB) {
            return listObjB.Any(x => Collision(objA, x));
        }
    }
}