﻿using Engine.Debugging;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public abstract class GameObject : RenderedObject {
        protected GameObject(Point point) {
            Point = point;
        }

        protected override void RenderCore(Point renderPoint) {
            DirectXManager.RenderTarget.DrawBitmap(image, new SharpDX.Mathematics.Interop.RawRectangleF((float)renderPoint.X, (float)renderPoint.Y, (float)renderPoint.X + image.Size.Width, (float)renderPoint.Y + image.Size.Height), 1f, SharpDX.Direct2D1.BitmapInterpolationMode.Linear);
            DebugHelper.ShowCoordsObject(this);
            DebugHelper.ShowBounds(this);
        }

        //Tick need abstract?
        public virtual void Tick() { }
        public virtual GameItem CreateGameItem() {
            return null;
        }
    }
}