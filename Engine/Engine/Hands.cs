﻿using System;
using Engine.Mathematics;

namespace Engine {
    public class Hands : Weapon {
        // Need static array for all weapons with times to shoot
        public Hands(GameObject owner, Game game, Point point, int radius) : base(owner, point, 2) {
            this.game = game;
            this.radius = radius;
        }

        readonly Game game;
        readonly int radius;

        public override void Shoot() {
            var blow = new Blow(new Point(owner.Point.X + (double)owner.GetBounds().Width / 2 - radius, owner.Point.Y + (double)owner.GetBounds().Height / 2 - radius), radius, DateTime.Now, owner);
            game.Controller.AddBlow(blow);
        }
    }
}