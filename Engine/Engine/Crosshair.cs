﻿using System.Drawing;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public class Crosshair : GameObject {
        public Crosshair(Point point) : base(point) {
            // GAME IS OPTIONAL (GAME FOR SPRITES)
            /////////////////////////////////////
            // LOGIC FOR GET PLAYER SPRITE
            var bitmap = new Bitmap(16, 16);
            using(var graphics = Graphics.FromImage(bitmap)) {
                graphics.FillEllipse(System.Drawing.Brushes.White, 0, 0, 16, 16);
                graphics.DrawImage(bitmap, 0, 0);
            }
            image = bitmap.ConvertImage();
            ///////////////////////
        }
    }
}