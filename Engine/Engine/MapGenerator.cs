﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine.Mathematics;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public class MapGenerator {
        public MapGenerator(Controller controller) {
            this.controller = controller;
        }

        readonly Controller controller;

        public IEnumerable<Tile> GenerateTiles() {
            var tiles = new List<Tile>();
            //TODO
            for(int x = 0; x < Game.Width; x += 32) {
                for(int y = 0; y < Game.Height; y += 32) {
                    var tile = new Tile(Textures.GetTexture("Grass"), new Point(x, y));
                    tiles.Add(tile);
                    controller.AddTile(tile);
                }
            }
            return tiles;
        }

        public IEnumerable<GameObject> GenerateMap(int countObstacle) {
            var objects = new List<GameObject>();
            var random = new Random();
            while(countObstacle > 0) {
                var obstacleSize = random.Next(5, 40);
                var offset = obstacleSize / 2;
                var x = random.Next(offset, Game.Width - offset);
                var y = random.Next(offset, Game.Height - offset);
                if(objects.FirstOrDefault(o => Math.Abs(o.Point.X - x) < MathHelper.Epsilon && Math.Abs(o.Point.Y - y) < MathHelper.Epsilon) != null)
                    continue;
                countObstacle--;
                var obstacle = new Obstacle(new Point(x, y), offset, offset);
                objects.Add(obstacle);
                controller.AddObstacle(obstacle);
            }
            objects.AddRange(GenerateTiles());
            return objects;
        }
    }
}