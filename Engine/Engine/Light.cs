﻿using System;
using SharpDX;
using SharpDX.Mathematics.Interop;

namespace Engine {
    public class Light : RenderedObject {
        const int MaxDayState = 160;
        const int MinDayState = 0;
        TimeSpan difference;
        DateTime nextTime;
        int state;
        int currentDayState;

        public void Setup(int lengthOfDayInMills) {
            // 255 need new logic
            difference = new TimeSpan(lengthOfDayInMills / 255 * TimeSpan.TicksPerMillisecond);
            state = 0;
            currentDayState = 1;
            nextTime = DateTime.Now + difference;
        }

        protected override void RenderCore(Mathematics.Point renderPoint) {
            DirectXManager.RenderTarget.FillRectangle(new RawRectangleF(0, 0, Game.Width, Game.Height), Brushes.GetBrush(new Color(0, 0, 0, state)));
        }

        public void Tick() {
            if((nextTime - DateTime.Now).TotalMilliseconds < 0) {
                nextTime = DateTime.Now + difference;
                state += currentDayState;
                if(state == MaxDayState || state == MinDayState)
                    currentDayState = -currentDayState;
            }
        }
    }
}