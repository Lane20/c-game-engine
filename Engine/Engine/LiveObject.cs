﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine.Mathematics;

namespace Engine {
    public abstract class LiveObject : GameObject {
        protected LiveObject(Point point, Game game)
            : base(point) {
            bullets = game.Controller.Bullets;
            Inventory = new Inventory(this, game);
        }

        readonly IEnumerable<Bullet> bullets;
        protected Action CollisionCallback;
        public Inventory Inventory { get; }

        public override void Tick() {
            if(Physics.Collision(this, bullets.ToArray())) {
                CollisionCallback?.Invoke();
            }
        }
    }
}