﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Engine.PathFinding;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public class Enemy : LiveObject {
        public Enemy(Point point, Game game, /*TEMP*/Player p) : base(point, game) {
            var bitmap = new Bitmap(32, 32);
            using(var graphics = Graphics.FromImage(bitmap)) {
                graphics.FillEllipse(System.Drawing.Brushes.Red, 0, 0, 32, 32);
                graphics.DrawImage(bitmap, 0, 0);
            }
            image = bitmap.ConvertImage();
            this.p = p;
        }

        /////TEMP
        readonly Player p;
        ///
        public LinkedList<Node> path;
        public Weapon Weapon { get; set; }

        public override void Tick() {
            // Here must be code A* algorithm
            PathFinder.FindPath(Point, p.Point, this);
            if(path.Count > 0) {
                Point.X = path.ElementAt(0).GridX * PathFinder.XPixels;
                Point.Y = path.ElementAt(0).GridY * PathFinder.YPixels;
            }
            Weapon.Shooting = !Physics.Collision(this, p);
            Weapon.Tick();
            base.Tick();
        }

        protected override void RenderCore(Point renderPoint) {
            if(path != null) {
                var bitmap = new Bitmap(8, 8);
                using(var pathGraphics = Graphics.FromImage(bitmap)) {
                    pathGraphics.FillRectangle(System.Drawing.Brushes.Red, 0, 0, 5, 5);
                    pathGraphics.DrawImage(bitmap, 0, 0);
                }
                var img = bitmap.ConvertImage();
                foreach(var node in path) {
                    var x = node.GridX * PathFinder.XPixels;
                    var y = node.GridY * PathFinder.YPixels;
                    DirectXManager.RenderTarget.DrawBitmap(img, new SharpDX.Mathematics.Interop.RawRectangleF(x, y, x + PathFinder.XPixels, y + PathFinder.YPixels), 1f, SharpDX.Direct2D1.BitmapInterpolationMode.Linear);
                }
            }
            base.RenderCore(renderPoint);
        }
    }
}