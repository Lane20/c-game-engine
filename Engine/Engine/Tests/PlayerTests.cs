﻿#if DEBUG
using Engine.Mathematics;
using NUnit.Framework;

namespace Engine.Tests {
    [TestFixture]
    public class PlayerTests : TestsBase {
        [Test]
        public void AddNewWeaponTest() {
            var game = new Game();
            var crosshair = new Crosshair(Point.Null);
            var player = new Player(Point.Null, game, crosshair);
            Assert.IsNull(player.Weapon);
            Assert.AreEqual(0, player.Weapons.Count);
            var weapon1 = new TestWeapon1(player);
            player.AddWeapon(weapon1);
            Assert.AreEqual(weapon1, player.Weapon);
            Assert.AreEqual(1, player.Weapons.Count);
            var weapon2 = new TestWeapon1(player);
            player.AddWeapon(weapon2);
            Assert.AreEqual(weapon2, player.Weapon);
            Assert.AreEqual(2, player.Weapons.Count);
        }

        [Test]
        public void ChangeWeaponTest() {
            var game = new Game();
            var crosshair = new Crosshair(Point.Null);
            var player = new Player(Point.Null, game, crosshair);
            var weapon1 = new TestWeapon1(player);
            var weapon2 = new TestWeapon1(player);
            player.AddWeapon(weapon1);
            player.AddWeapon(weapon2);
            Assert.AreEqual(weapon2, player.Weapon);
            player.ChangeWeapon();
            Assert.AreEqual(weapon1, player.Weapon);
            player.ChangeWeapon();
            Assert.AreEqual(weapon2, player.Weapon);
            player.ChangeWeapon(false);
            Assert.AreEqual(weapon1, player.Weapon);
            player.ChangeWeapon(false);
            Assert.AreEqual(weapon2, player.Weapon);
        }
    }
}
#endif