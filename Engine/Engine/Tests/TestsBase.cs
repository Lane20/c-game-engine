﻿#if DEBUG
using System.Runtime.InteropServices;
using Engine.Core;
using NUnit.Framework;
using SharpDX;
using SharpDX.Direct2D1;

namespace Engine.Tests {
    public class TestsBase {
        [SetUp]
        public void SetUp() {
            DirectXManager.Initialize();
        }

        [TearDown]
        public void TearDown() {
            DisposeHelper.DisposeStorage();
        }

        protected byte[] GetPixelsData(Bitmap image) {
            var deviceContext2d = DirectXManager.RenderTarget.QueryInterface<DeviceContext>();
            var bitmapProperties = new BitmapProperties1 {
                BitmapOptions = BitmapOptions.CannotDraw | BitmapOptions.CpuRead,
                PixelFormat = image.PixelFormat
            };
            var bitmap1 = new Bitmap1(deviceContext2d, new Size2((int)image.Size.Width, (int)image.Size.Height), bitmapProperties);
            bitmap1.CopyFromBitmap(image);
            var map = bitmap1.Map(MapOptions.Read);
            var size = (int)image.Size.Width * (int)image.Size.Height * 4;
            var bytes = new byte[size];
            Marshal.Copy(map.DataPointer, bytes, 0, size);
            bitmap1.Unmap();
            bitmap1.Dispose();
            deviceContext2d.Dispose();
            return bytes;
        }

        protected Color4 GetPixel(Bitmap image, byte[] pixelsData, int x, int y) {
            var position = (y * (int)image.Size.Width + x) * 4;
            return new Color4(pixelsData[position], pixelsData[position + 1], pixelsData[position + 2], pixelsData[position + 3]);
        }
    }
}
#endif