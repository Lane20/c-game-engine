﻿#if DEBUG
using NUnit.Framework;
using SharpDX.Direct2D1;

namespace Engine.Tests {
    [TestFixture]
    public class GridTests : TestsBase {
        [Test]
        public void SetupSettingsTest() {
            var grid1 = new TestGrid();
            grid1.SetupSettings(32, 32);
            var grid2 = new TestGrid();
            grid2.SetupSettings(64, 64);
            Assert.IsFalse(EqualsGrids(grid1.Image, grid2.Image));
            grid1.SetupSettings(64, 64);
            Assert.IsTrue(EqualsGrids(grid1.Image, grid2.Image));
        }

        class TestGrid : Grid {
            public Bitmap Image => image;
        }

        bool EqualsGrids(Bitmap grid1, Bitmap grid2) {
            var pixelsData1 = GetPixelsData(grid1);
            var pixelsData2 = GetPixelsData(grid2);
            for(int x = 0; x < Game.Width; x++)
                if(GetPixel(grid1, pixelsData1, x, 0) != GetPixel(grid2, pixelsData2, x, 0))
                    return false;
            for(int y = 0; y < Game.Height; y++)
                if(GetPixel(grid1, pixelsData1, 0, y) != GetPixel(grid2, pixelsData2, 0, y))
                    return false;
            return true;
        }
    }
}
#endif