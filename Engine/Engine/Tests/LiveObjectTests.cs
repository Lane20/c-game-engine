﻿#if DEBUG
using Engine.Mathematics;
using NUnit.Framework;

namespace Engine.Tests {
    [TestFixture]
    public class LiveObjectTests : TestsBase {
        [Test]
        public void CollisionWithBulletsTest() {
            var game = new Game();
            var wasCollision = false;
            var liveObject = new TestLiveObject(game, () => wasCollision = true);
            game.Controller.AddBullet(new Bullet(Point.Null, Point.Null, 8, 8));
            liveObject.Tick();
            Assert.IsTrue(wasCollision);
        }
    }
}
#endif