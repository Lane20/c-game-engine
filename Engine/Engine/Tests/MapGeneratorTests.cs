﻿#if DEBUG
using System;
using System.Linq;
using NUnit.Framework;

namespace Engine.Tests {
    [TestFixture]
    public class MapGeneratorTests : TestsBase {
        [Test]
        public void GenerateTilesTest() {
            var count = Math.Ceiling(Game.Width / 32f) * Math.Ceiling(Game.Height / 32f);
            var controller = new Controller();
            var mapCreator = new MapGenerator(controller);
            var objects = mapCreator.GenerateTiles();
            Assert.AreEqual(count, objects.Count());
        }

        [Test]
        public void GenerateMapTest() {
            var countObstacles = 7;
            var count = Math.Ceiling(Game.Width / 32f) * Math.Ceiling(Game.Height / 32f);
            var controller = new Controller();
            var mapCreator = new MapGenerator(controller);
            var objects = mapCreator.GenerateMap(countObstacles);
            Assert.AreEqual(countObstacles + count, objects.Count());
        }
    }
}
#endif