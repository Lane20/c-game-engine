﻿#if DEBUG
using System;
using Engine.Mathematics;

namespace Engine.Tests {
    class TestGameObject : GameObject {
        public TestGameObject(Point point) : base(point) { }
    }

    class TestLiveObject : LiveObject {
        public TestLiveObject(Game game, Action collisionCallback) : base(Point.Null, game) {
            image = new SharpDX.Direct2D1.Bitmap(
                DirectXManager.RenderTarget,
                new SharpDX.Size2(1, 1),
                new SharpDX.Direct2D1.BitmapProperties(
                    new SharpDX.Direct2D1.PixelFormat(
                        SharpDX.DXGI.Format.B8G8R8A8_UNorm,
                        SharpDX.Direct2D1.AlphaMode.Premultiplied
                    )
                )
            );
            CollisionCallback = collisionCallback;
        }
    }

    class TestWeapon1 : Weapon {
        public TestWeapon1(GameObject obj) : base(obj, Point.Null, 1) { }
        public override void Shoot() { }
    }

    class TestWeapon2 : Weapon {
        public TestWeapon2(GameObject obj) : base(obj, Point.Null, 1) { }
        public override void Shoot() { }
    }

    class TestItem : GameItem {
        public TestItem() : base("Error") { }

        public override GameObject CreateGameObject(Point point) {
            return new TestGameObject(point);
        }
    }
}
#endif