﻿#if DEBUG
using Engine.Mathematics;
using NUnit.Framework;

namespace Engine.Tests {
    [TestFixture]
    public class MathTests {
        [Test]
        public void GetVectorLenghtTest() {
            var p1 = new Point(0, 0);
            var p2 = new Point(0, 4);
            var vector = new Vector(p1, p2);
            var result = vector.Lenght;
            Assert.AreEqual(4, result);
        }

        [Test]
        public void GetStepsTest() {
            var p1 = new Point(0, 0);
            var p2 = new Point(0, 110);
            var vector = new Vector(p1, p2);
            var result = MathHelper.GetSteps(vector, 10);
            Assert.AreEqual(11, result);
            p2 = new Point(40, 30);
            vector = new Vector(p1, p2);
            result = MathHelper.GetSteps(vector, 10);
            Assert.AreEqual(5, result, 0);
        }

        [Test]
        public void GetNextCoordTest() {
            var p1 = new Point(0, 0);
            var p2 = new Point(10, 10);
            var x = MathHelper.GetNextCoord(p1.X, p2.X, 10);
            var y = MathHelper.GetNextCoord(p1.Y, p2.Y, 10);
            Assert.AreEqual(1, x);
            Assert.AreEqual(1, y);
            p2 = new Point(2, 4);
            x = MathHelper.GetNextCoord(p1.X, p2.X, 2);
            y = MathHelper.GetNextCoord(p1.Y, p2.Y, 2);
            Assert.AreEqual(1, x);
            Assert.AreEqual(2, y);
            p2 = new Point(0, 10);
            x = MathHelper.GetNextCoord(p1.X, p2.X, 2);
            y = MathHelper.GetNextCoord(p1.Y, p2.Y, 2);
            Assert.AreEqual(0, x);
            Assert.AreEqual(5, y);
            p2 = new Point(0, 0);
            x = MathHelper.GetNextCoord(p1.X, p2.X, 70);
            y = MathHelper.GetNextCoord(p1.Y, p2.Y, 70);
            Assert.AreEqual(0, x);
            Assert.AreEqual(0, y);
        }
    }
}
#endif