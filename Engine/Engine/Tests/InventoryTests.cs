﻿#if DEBUG
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace Engine.Tests {
    [TestFixture]
    public class InventoryTests : TestsBase {
        [OneTimeSetUp]
        public void SetUp1() {

        }

        [OneTimeTearDown]
        public void Ss() {

        }


        [Test]
        public void PutTest() {
            var inventory = new Inventory(null, null);
            Assert.AreEqual(0, inventory.Items.Count());
            inventory.Put(new TestItem());
            Assert.AreEqual(1, inventory.Items.Count());
        }

        [Test]
        public void PullTest() {
            var game = new Game();
            var liveObject = new TestLiveObject(game, null);
            var inventory = new Inventory(liveObject, game);
            inventory.Put(new TestItem());
            var gameItems = (IList<GameObject>)typeof(Controller).GetField("gameItems", BindingFlags.NonPublic | BindingFlags.Instance)?.GetValue(game.Controller);
            Assert.IsNotNull(gameItems);
            Assert.AreEqual(0, gameItems.Count);
            Assert.AreEqual(1, inventory.Items.Count());
            inventory.Pull(0);
            Assert.AreEqual(1, gameItems.Count);
            Assert.AreEqual(0, inventory.Items.Count());
        }

        [Test]
        public void PutIfFullTest() {
            var inventory = new Inventory(null, null, 1);
            Assert.IsTrue(inventory.Put(new TestItem()));
            Assert.IsFalse(inventory.Put(new TestItem()));
        }
    }
}
#endif