﻿using System.Drawing;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public class Obstacle : GameObject {
        // Need move this to controller logic
        public Obstacle(Point point, double width, double height) : base(point) {
            var bitmap = new Bitmap((int)width, (int)height/*, PixelFormat.......*/);
            using(var graphics = Graphics.FromImage(bitmap)) {
                graphics.FillEllipse(System.Drawing.Brushes.White, 0, 0, (int)width, (int)height);
                graphics.DrawImage(bitmap, 0, 0);
            }
            image = bitmap.ConvertImage();
        }
    }
}