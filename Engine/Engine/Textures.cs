﻿using System.Collections.Generic;
using System.Reflection;
using SharpDX.Direct2D1;

namespace Engine {
    public static class Textures {
        static Textures() {
            Storage = new Dictionary<string, Bitmap>();
            Add("Error", DirectXManager.LoadBitmap(Assembly.GetExecutingAssembly(), "Images.Error.png"));
        }

        static readonly IDictionary<string, Bitmap> Storage;

        public static Bitmap GetTexture(string key) {
            Bitmap result;
            return Storage.TryGetValue(key, out result) ? result : Storage["Error"];
        }

        public static void Add(string key, Bitmap texture) {
            Storage.Add(key, texture);
        }
    }
}