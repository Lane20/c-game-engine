﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Engine.Debugging;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public class Player : LiveObject {
        public Player(Point point, Game game, Crosshair crosshair) : base(point, game) {
            // LOGIC FOR GET PLAYER SPRITE
            var bitmap = new Bitmap(32, 32/*, PixelFormat.Format32bppArgb*/);
            using(var graphics = Graphics.FromImage(bitmap)) {
                graphics.FillRectangle(System.Drawing.Brushes.White, 0, 0, 32, 32);
                graphics.DrawImage(bitmap, 0, 0);
            }
            image = bitmap.ConvertImage();
            Crosshair = crosshair;
            Speed = 1;
            obstacles = game.Controller.Obstacles;
            Weapons = new List<Weapon>();
            this.game = game;
            achievements = new List<Achievement>();
        }

        public double VelX { private get; set; }
        public double VelY { private get; set; }
        public double Speed { private get; set; }
        public Crosshair Crosshair { get; }
        public Weapon Weapon { get; set; }
        //TODO: Maybe it must been public?
        public IList<Weapon> Weapons { get; }
        //
        readonly IEnumerable<Obstacle> obstacles;
        //Bad code
        readonly List<Achievement> achievements;
        public IEnumerable<Achievement> Achievements => achievements;
        public int AchievementCount => achievements.Count;
        //End Bad code
        readonly Game game;

        public void CheckAchievements() {
            achievements.AddRange(game.AchievementRegistrator.Achievements.Where(x => x.IsComplete() && !achievements.Contains(x)));
        }

        public override void Tick() {
            Point.X += VelX * Speed;
            if(Point.X <= 0)
                Point.X = 0;
            else if(Point.X >= Game.Width - Width)
                Point.X = Game.Width - Width;
            Point.Y += VelY * Speed;
            if(Point.Y <= 0)
                Point.Y = 0;
            else if(Point.Y >= Game.Height - Height)
                Point.Y = Game.Height - Height;
            Weapon.Tick();
            if(Physics.Collision(this, obstacles.ToArray())) {
                Point.X -= VelX * Speed;
                Point.Y -= VelY * Speed;
            }
            base.Tick();
        }

        public void AddWeapon(Weapon weapon) {
            Weapons.Add(weapon);
            Weapon = weapon;
        }

        public void RemoveWeapon(Weapon weapon) { Weapons.Remove(weapon); }

        protected override void RenderCore(Point renderPoint) {
            //graphics.TranslateTransform((float)-Point.X, (float)-Point.Y);
            //graphics.RotateTransform((float)MathHelper.RadianToDegree(MathHelper.GetRotateTheta(Point, crosshair.Point)), MatrixOrder.Append);
            //graphics.TranslateTransform((float)Point.X, (float)Point.Y, MatrixOrder.Append);
            DebugHelper.ShowRegionWhereCanPickUpItems(this, 64);
            base.RenderCore(renderPoint);
            //graphics.ResetTransform();
        }

        public void ChangeWeapon(bool next = true) {
            var index = Weapons.IndexOf(Weapon) + (next ? 1 : -1);
            Weapon = Weapons[index < 0 ? Weapons.Count - 1 : index % Weapons.Count];
        }
    }
}