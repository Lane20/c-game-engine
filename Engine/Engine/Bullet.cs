﻿using System;
using System.Drawing;
using Engine.Mathematics;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public class Bullet : GameObject {

        public Bullet(Point point, Point end, int width, int height) : base(point) {
            // GAME IS NATIVE (GAME FOR SPRITES)
            /////////////////////////////////////
            // LOGIC FOR GET PLAYER SPRITE
            var bitmap = new Bitmap(width, height);
            using(var graphics = Graphics.FromImage(bitmap)) {
                graphics.FillEllipse(System.Drawing.Brushes.White, 0, 0, width, height);
                graphics.DrawImage(bitmap, 0, 0);
            }
            image?.Dispose();
            image = bitmap.ConvertImage();
            bitmap.Dispose();
            ///////////////////////
            this.end = end;
        }

        /////////////////////TEMP
        double tx;
        double ty;
        //////////////////
        readonly Point end;

        public override void Tick() {
            Vector vector = Vector.Empty;
            if(Math.Abs(tx) < MathHelper.Epsilon && Math.Abs(ty) < MathHelper.Epsilon)
                vector = new Vector(Point, new Point(end));
            if(Math.Abs(tx) < MathHelper.Epsilon) {
                tx = MathHelper.GetNextCoord(Point.X, end.X, MathHelper.GetSteps(vector, 15));
            }
            if(Math.Abs(ty) < MathHelper.Epsilon) {
                ty = MathHelper.GetNextCoord(Point.Y, end.Y, MathHelper.GetSteps(vector, 15));
            }
            Point.X = Point.X + tx;
            Point.Y = Point.Y + ty;
        }
    }
}