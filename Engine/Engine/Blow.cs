﻿using System;
using System.Drawing;
using Point = Engine.Mathematics.Point;

namespace Engine {
    public class Blow : GameObject {
        public Blow(Point point, int radius, DateTime timeCreate, GameObject owner) : base(point) {
            var pen = new Pen(Color.Blue, 4);
            var bitmap = new Bitmap(radius * 2, radius * 2);
            using(var graphics = Graphics.FromImage(bitmap)) {
                graphics.DrawEllipse(pen, 0, 0, radius * 2, radius * 2);
                graphics.DrawImage(bitmap, 0, 0);
            }
            image?.Dispose();
            image = bitmap.ConvertImage();
            bitmap.Dispose();
            TimeCreate = timeCreate;
            this.owner = owner;
            this.radius = radius;
        }

        readonly GameObject owner;
        readonly int radius;
        public DateTime TimeCreate { get; }

        public override void Tick() {
            Point.X = owner.Point.X + (double)owner.GetBounds().Width / 2 - radius;
            Point.Y = owner.Point.Y + (double)owner.GetBounds().Height / 2 - radius;
        }
    }
}