﻿namespace SQLiteConnector {
    public enum QueryType {
        None,
        Insert,
        Select,
        Create,
        Delete,
        Update
    }
}