﻿namespace SQLiteConnector {
    public sealed class QueryRecordParameter<T> {
        internal static QueryRecordParameter<T> Create(string columnName, T value) {
            return new QueryRecordParameter<T>(columnName, value);
        }

        QueryRecordParameter(string columnName, T value) {
            Info = new QueryRecordParameterInfo(typeof(T)) {
                ColumnName = columnName,
                Value = value
            };
        }

        public QueryRecordParameterInfo Info { get; }
    }
}