﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using SQLiteConnector.Queries;
using SQLiteConnector.Queries.Base;

namespace SQLiteConnector {
    public sealed class Connection {
        internal static Connection Create(string dataSource) {
            return new Connection(dataSource);
        }

        Connection(string dataSource) {
            queries = new Dictionary<string, Query>();
        }

        readonly IDictionary<string, Query> queries;

        public Query Insert(string queryName) {
            if(string.IsNullOrEmpty(queryName))
                throw new ArgumentException("Name can't be null or empty");

            var query = InsertQuery.Create(this);
            queries.Add(queryName, query);
            return query;
        }

        public Connection ExecuteQuery(string name) {
            if(queries.ContainsKey(name))
                throw new ArgumentException($"Name {name} not found");

            queries[name].Execute();
            return this;
        }

        public Connection ExecuteQueries(QueryType? queryType = null) {
            foreach(var query in queries.Values) {
                if(!queryType.HasValue || (queryType != QueryType.None && query.QueryType == queryType))
                    query.Execute();
            }
            return this;
        }

        string BuildConnectionString(string dataSource) {
            var connectionStringBuilder = new SQLiteConnectionStringBuilder() {
                DataSource = dataSource
            };
            return connectionStringBuilder.ConnectionString;
        }
    }
}