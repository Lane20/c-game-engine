﻿using System;
using System.Collections.Generic;
using SQLiteConnector.Queries.Base;

namespace SQLiteConnector.Queries {
    public sealed class InsertQuery : Query {
        internal static InsertQuery Create(Connection connection) {
            return new InsertQuery(connection);
        }

        InsertQuery(Connection connection) : base(connection, QueryType.Insert) {
            records = new List<QueryRecord>();
        }

        readonly IList<QueryRecord> records;

        public QueryRecord Record() {
            ValidateColumns();

            var record = QueryRecord.Create(this);
            records.Add(record);
            return record;
        }

        public override void Execute() {
            throw new NotImplementedException();
        }
    }
}