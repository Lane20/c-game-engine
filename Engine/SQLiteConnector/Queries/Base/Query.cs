﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SQLiteConnector.Queries.Base {
    public abstract class Query {
        protected Query(Connection connection, QueryType queryType) {
            this.connection = connection;
            columnsMap = new Dictionary<string, Type>();
            QueryType = queryType;
        }

        readonly Connection connection;
        readonly IDictionary<string, Type> columnsMap;

        public QueryType QueryType { get; }

        public Query RegisterColumn<T>(string columnName) {
            return RegisterColumn(columnName, typeof(T));
        }

        public Query RegisterColumn(string columnName, Type columnType) {
            columnsMap.Add(columnName, columnType);
            return this;
        }

        public Connection EndCreateQuery() {
            return connection;
        }

        public abstract void Execute();

        protected void ValidateColumns() {
            if(IsEmpty()) {
                throw new ArgumentException("No registered columns");
            }
        }

        protected void ValidateColumn(string columnName) {
            if(!columnsMap.ContainsKey(columnName)) {
                throw new ArgumentNullException($"Column {columnName} not found");
            }
        }

        internal Type GetColumnType(string columnName) {
            ValidateColumn(columnName);

            return columnsMap[columnName];
        }

        internal bool IsEmpty() {
            return !columnsMap.Any();
        }
    }
}