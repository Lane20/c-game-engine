﻿using System;
using System.Collections.Generic;
using SQLiteConnector.Queries.Base;

namespace SQLiteConnector.Queries {
    public class DeleteQuery : Query {
        internal static DeleteQuery Create(Connection connection) {
            return new DeleteQuery(connection);
        }

        DeleteQuery(Connection connection) : base(connection, QueryType.Delete) {
            tables = new List<QueryTable>();
        }

        readonly IList<QueryTable> tables;

        public QueryTable From(string tableName) {
            var table = QueryTable.Create(this, tableName);
            tables.Add(table);
            return table;
        }

        public override void Execute() {
            throw new NotImplementedException();
        }
    }
}