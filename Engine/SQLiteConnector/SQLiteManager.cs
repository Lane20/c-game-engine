﻿using System;

namespace SQLiteConnector {
    public sealed class SQLiteManager {
        public static SQLiteManager New() {
            return new SQLiteManager();
        }

        SQLiteManager() { }

        public Connection OpenConnection(string dataSource) {
            if(string.IsNullOrEmpty(dataSource))
                throw new ArgumentException($"Data file {dataSource} not found");

            return Connection.Create(dataSource);
        }
    }
}