﻿using System.Collections.Generic;
using SQLiteConnector.Queries.Base;

namespace SQLiteConnector {
    public class QueryTable {
        internal static QueryTable Create(Query query, string tableName) {
            return new QueryTable(query, tableName);
        }

        QueryTable(Query query, string tableName) {
            this.query = query;
            TableName = tableName;
            whereStorage = new Dictionary<string, QueryCondition>();
        }

        readonly Query query;
        readonly IDictionary<string, QueryCondition> whereStorage;

        internal string TableName { get; }

        public QueryTable Where(string columnName, LogicalOperator logicalOperator, object equalsValue) {
            whereStorage.Add(columnName, QueryCondition.Create(logicalOperator, equalsValue));
            return this;
        }

        public Query EndFrom() {
            return query;
        }
    }
}