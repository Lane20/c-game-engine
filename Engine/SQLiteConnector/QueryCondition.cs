﻿namespace SQLiteConnector {
    class QueryCondition {
        internal static QueryCondition Create(LogicalOperator logicalOperator, object equalsValue) {
            return new QueryCondition(logicalOperator, equalsValue);
        }

        QueryCondition(LogicalOperator logicalOperator, object equalsValue) {
            LogicalOperator = logicalOperator;
            EqualsValue = equalsValue;
        }

        public LogicalOperator LogicalOperator { get; }
        public object EqualsValue { get; }
    }
}