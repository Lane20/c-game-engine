﻿using System;

namespace SQLiteConnector {
    public sealed class QueryRecordParameterInfo {
        public QueryRecordParameterInfo(Type type) {
            Type = type;
        }

        public string ColumnName { get; set; }
        public Type Type { get; }
        public object Value { get; set; }
    }
}