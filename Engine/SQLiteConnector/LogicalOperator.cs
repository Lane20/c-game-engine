﻿namespace SQLiteConnector {
    public enum LogicalOperator {
        Equal,
        NotEqual,
        LessThan,
        GreaterThan,
        LessThanOrEqual,
        GreaterThanOrEqual
    }
}