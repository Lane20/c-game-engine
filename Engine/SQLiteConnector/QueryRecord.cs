﻿using System;
using System.Collections.Generic;
using SQLiteConnector.Queries.Base;

namespace SQLiteConnector {
    public sealed class QueryRecord {
        internal static QueryRecord Create(Query query) {
            return new QueryRecord(query);
        }

        QueryRecord(Query query) {
            this.query = query;
            parameters = new List<QueryRecordParameterInfo>();
        }

        readonly Query query;
        readonly IList<QueryRecordParameterInfo> parameters;

        public QueryRecord AddParameter<T>(string columnName, T value) {
            if(typeof(T) != query.GetColumnType(columnName))
                throw new ArgumentException($"Wrong value type for \"{columnName}\" ({query.GetColumnType(columnName).Name})");

            var parameter = QueryRecordParameter<T>.Create(columnName, value);
            parameters.Add(parameter.Info);
            return this;
        }

        public Query EndAddRecord() {
            return query;
        }
    }
}