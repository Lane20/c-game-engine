﻿using SharpDX;

namespace DirectXExamples.Examples.DirectInput {
    public struct VertexType {
        public Vector3 Position;
        public Vector2 Texture;
    }
}