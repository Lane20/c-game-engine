﻿using SharpDX;

namespace DirectXExamples.Examples.FpsCpuUsageTimes {
    public struct VertexType {
        public Vector3 Position;
        public Vector2 Texture;
    }
}