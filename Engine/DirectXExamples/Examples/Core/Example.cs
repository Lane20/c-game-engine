﻿using System;

namespace DirectXExamples.Examples.Core {
    public abstract class Example {
        protected const int DefaultWindowWidth = 1280;
        protected const int DefaultWindowHeight = 720;

        public void Run() {
            try {
                if(!Initialize())
                    throw new InvalidOperationException();
                Show();
            } finally {
                Shutdown();
            }
        }

        protected abstract bool Initialize();
        protected abstract void Show();
        protected abstract void Shutdown();
    }
}