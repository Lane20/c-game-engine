﻿using SharpDX;

namespace DirectXExamples.Examples.FontEngine {
    public struct VertexType {
        public Vector3 Position;
        public Vector2 Texture;
    }
}