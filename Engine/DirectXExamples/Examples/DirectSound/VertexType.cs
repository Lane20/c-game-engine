﻿using SharpDX;

namespace DirectXExamples.Examples.DirectSound {
    public struct VertexType {
        public Vector3 Position;
        public Vector2 Texture;
    }
}