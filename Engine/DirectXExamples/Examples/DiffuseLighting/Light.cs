﻿using SharpDX;

namespace DirectXExamples.Examples.DiffuseLighting {
    public class Light {
        public Color4 DiffuseColor { get; set; }
        public Vector3 Direction { get; set; }
    }
}