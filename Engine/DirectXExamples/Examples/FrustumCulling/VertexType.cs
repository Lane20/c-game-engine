﻿using SharpDX;

namespace DirectXExamples.Examples.FrustumCulling {
    public struct VertexType {
        public Vector3 Position;
        public Vector2 Texture;
    }
}