﻿using SharpDX;

namespace DirectXExamples.Examples.FrustumCulling {
    public class Light {
        public Color4 DiffuseColor { get; set; }
        public Vector3 Direction { get; set; }
    }
}