﻿using DirectXExamples.Examples.AmbientLighting;
using DirectXExamples.Examples.BuffersShadersHLSL;
using DirectXExamples.Examples.Core;
using DirectXExamples.Examples.DiffuseLighting;
using DirectXExamples.Examples.DirectInput;
using DirectXExamples.Examples.DirectSound;
using DirectXExamples.Examples.FontEngine;
using DirectXExamples.Examples.FpsCpuUsageTimes;
using DirectXExamples.Examples.FrustumCulling;
using DirectXExamples.Examples.InitializingDirectX;
using DirectXExamples.Examples.ModelRendering3D;
using DirectXExamples.Examples.Rendering2D;
using DirectXExamples.Examples.SpecularLighting;
using DirectXExamples.Examples.Texturing;

namespace DirectXExamples {
    class Program {
        static void Main(string[] args) {
            Menu.RegisterItem(InitializingDirectX.Title, ExampleManager.Run<InitializingDirectX>);
            Menu.RegisterItem(BuffersShadersHLSL.Title, ExampleManager.Run<BuffersShadersHLSL>);
            Menu.RegisterItem(Texturing.Title, ExampleManager.Run<Texturing>);
            Menu.RegisterItem(DiffuseLighting.Title, ExampleManager.Run<DiffuseLighting>);
            Menu.RegisterItem(ModelRendering3D.Title, ExampleManager.Run<ModelRendering3D>);
            Menu.RegisterItem(AmbientLighting.Title, ExampleManager.Run<AmbientLighting>);
            Menu.RegisterItem(SpecularLighting.Title, ExampleManager.Run<SpecularLighting>);
            Menu.RegisterItem(Rendering2D.Title, ExampleManager.Run<Rendering2D>);
            Menu.RegisterItem(FontEngine.Title, ExampleManager.Run<FontEngine>);
            Menu.RegisterItem(DirectInput.Title, ExampleManager.Run<DirectInput>);
            Menu.RegisterItem(DirectSound.Title, ExampleManager.Run<DirectSound>);
            Menu.RegisterItem(FpsCpuUsageTimes.Title, ExampleManager.Run<FpsCpuUsageTimes>);
            Menu.RegisterItem(FrustumCulling.Title, ExampleManager.Run<FrustumCulling>);
            Menu.Show();
        }
    }
}